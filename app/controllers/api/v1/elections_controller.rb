class Api::V1::ElectionsController < ApplicationController

	def index
    _sortable = params[:sort].present? ? "#{params[:sort]} #{sort_direction}" : 'created_at desc'
    @page = params[:page] || 1
    @per = params[:per] || 10
    @elections = Election.order(_sortable).includes(:category)
    @elections = @elections.search(params[:name], "by_name") if params[:name].present?
    @elections = @elections.search(params[:category_id], "by_cat") if params[:category_id].present?
    @total_items = @elections.count
    @elections  = @elections.page(@page).per(@per)
  end

  private
    def sort_direction
      %w[asc desc].include?(params[:sort_dir]) ? params[:sort_dir] : 'asc'
    end
end

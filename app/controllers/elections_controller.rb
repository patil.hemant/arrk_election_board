class ElectionsController < ApplicationController
	layout :resolve_layout

	def index
		@elections = Election.all
	end

	private

		def resolve_layout
      case action_name
      when "index"
        "react" # Load react layout for index action
      else
        "application" # Load application layout for other actions
      end
    end 
end

import React from "react";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Elections from "./views/elections";

const App = () => {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" exact component={Elections} />
          <Route path="/elections" exact component={Elections} />
</Switch>
      </Router>
    </>
  )
}

export default App;

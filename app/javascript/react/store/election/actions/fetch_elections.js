import axios from 'axios';
import {
  ELECTIONS_FETCH_REQUEST,
  ELECTIONS_FETCH_SUCCESS,
  ELECTIONS_FETCH_FAILURE
} from '../constants';

function requesting() {
  return {
    type: ELECTIONS_FETCH_REQUEST
  };
}
function requestSuccess(data) {
  return {
    type: ELECTIONS_FETCH_SUCCESS,
    payload: { data: data.data, meta: data.meta }
  };
}
function requestError(error) {
  return {
    type: ELECTIONS_FETCH_FAILURE,
    payload: error
  };
}

export default function fetchElections(args = {}) {
  const { page, sort, sort_dir, name, category_id } = args;

  const params = {};
  if (page !== 'undefined' || typeof (page) !== 'undefined') {
    params["page"] = page
  }
  if (sort !== 'undefined' || typeof (sort) !== 'undefined') {
    params["sort"] = sort
  }
  if (sort_dir !== 'undefined' || typeof (sort_dir) !== 'undefined') {
    params["sort_dir"] = sort_dir
  }
  if (name !== 'undefined' || typeof (name) !== 'undefined') {
    params["name"] = name
  }
  if (category_id !== 'undefined' || typeof (category_id) !== 'undefined') {
    params["category_id"] = category_id
  }
  return function (dispatch) {
    dispatch(requesting());
    axios.get(`/api/v1/elections.json`, { params })
      .then(response => {
        dispatch(requestSuccess(response.data));
      })
      .catch(error => {
        dispatch(requestError('Oops!! We are unable to load data, please try after sometime.'));
      });
  };
}

export const ELECTIONS_FETCH_REQUEST = 'fetch_elections_request';
export const ELECTIONS_FETCH_SUCCESS = 'fetch_elections_success';
export const ELECTIONS_FETCH_FAILURE = 'fetch_elections_failure';
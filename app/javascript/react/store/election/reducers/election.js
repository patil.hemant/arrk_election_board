import {
  ELECTIONS_FETCH_REQUEST,
  ELECTIONS_FETCH_SUCCESS,
  ELECTIONS_FETCH_FAILURE
} from '../constants';

const INITIAL_STATE = {
  all: [],
  meta: null,
  isFetching: false,
  isError: false,
  errorMessage: null
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case ELECTIONS_FETCH_REQUEST:
      return {
        ...state,
        isFetching: true,
        isError: false,
        errorMessage: null
      };
    case ELECTIONS_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        all: action.payload.data,
        meta: action.payload.meta
      };
    case ELECTIONS_FETCH_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        errorMessage: action.payload
      };
    default:
      return state;
  }
}

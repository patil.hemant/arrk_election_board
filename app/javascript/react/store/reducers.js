import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import electionReducer from './election/reducers/election';

const rootReducer = combineReducers({
  form: formReducer,
  election: electionReducer
});

export default rootReducer;

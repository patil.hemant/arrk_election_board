import React, { Component } from 'react';
import { connect } from 'react-redux';
import fetchElections from '../../store/election/actions/fetch_elections';
import SmartTable from './SmartTable';

class ElectionsContainer extends Component {
  constructor(props) {
    super(props);
    this.handlePageNumberClick = this.handlePageNumberClick.bind(this);
    this.handleSorting = this.handleSorting.bind(this);
    this.state = {
      sort: null,
      sort_dir: null
    }
  }

  componentDidMount() {
    this.props.fetchElections();
  }

  handlePageNumberClick(page) {
    const args = { page: page };
    const { sort, sort_dir } = this.state;
    if(sort && sort_dir) {
      args["sort"] = sort
      args["sort_dir"] = sort_dir
    }
    const formValues = this.props.filterForm;
    if(formValues.values && formValues.values.name) {
      args["name"] = formValues.values.name
    }
    if(formValues.values && formValues.values.category_id) {
      args["category_id"] = formValues.values.category_id
    }
    this.props.fetchElections(args);
  }

  handleSorting(sort, sort_dir) {
    this.setState({ sort })
    this.setState({ sort_dir })
    const args = {};
    if(sort && sort_dir) {
      args["sort"] = sort
      args["sort_dir"] = sort_dir
    }
    const formValues = this.props.filterForm;
    if(formValues.values && formValues.values.name) {
      args["name"] = formValues.values.name
    }
    if(formValues.values && formValues.values.category_id) {
      args["category_id"] = formValues.values.category_id
    }
    this.props.fetchElections(args);
  }

  render() {
    const {
      isFetching,
      isError,
      errorMessage,
      elections,
      electionMeta
    } = this.props;
    const isEmpty = elections.length === 0;

    if (isEmpty && isFetching) {
      return <span>Loading...</span>;
    }
    if (isError) {
      return <span>{errorMessage}</span>;
    }
    if (isEmpty) {
      return <div className ="default-div"><h5>No election found with the current applied filters.</h5><h6>Try applying different filters.</h6></div>;
    }
    return (
      <SmartTable
        elections={elections}
        electionMeta={electionMeta}
        handlePageNumberClick={this.handlePageNumberClick}
        handleSorting={this.handleSorting}
    />
  );
  }
}
function mapStateToProps(state) {
  return {
    elections: state.election.all,
    electionMeta: state.election.meta,
    isFetching: state.election.isFetching,
    isError: state.election.isError,
    errorMessage: state.election.errorMessage,
    filterForm: state.form.ElectionFilterForm
  };
}
export default connect(mapStateToProps, { fetchElections })(ElectionsContainer);


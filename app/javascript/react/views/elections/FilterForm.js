import React from 'react';
import { Form, Field } from 'redux-form';
import Grid from '@material-ui/core/Grid';

const FilterForm = props => {

  const { handleSubmit, submitForm } = props;
  debugger
  return (
    <Form onSubmit={handleSubmit(submitForm)}>
      <Grid container spacing={4}>
        <Grid item>
          <Field
            name="name"
            component="input"
            type="text"
            placeholder="Search by name"
          />
        </Grid>

        <Grid item>
          <Field name="category_id" component="select">
            <option> Select Election Category</option>
            <option value="College Representative">College Representative</option>
            <option value="President">President</option>
            <option value="Vice President">Vice President</option>
          </Field>
        </Grid>
        
      </Grid>
    </Form>
  );
};
export default FilterForm;

import React from 'react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';


// Relative imports
import fetchElections from '../../store/election/actions/fetch_elections';
import FilterForm from './FilterForm';

const FilterFormContainer = props => {

  const { handleSubmit } = props;

  const submitForm = ({ name, category_id }) => {
    props.fetchElections({ name, category_id });
  }

  return (
    <FilterForm
      submitForm={submitForm}
      handleSubmit={handleSubmit}
    />
  );
}

const FilterFormReduxForm = reduxForm({
  form: 'ElectionFilterForm',
  onChange: (values, dispatch, props, previousValues) => {
    props.submit();
  }
})(FilterFormContainer);

const FilterFormConnectReduxForm = connect(
  null,
  { fetchElections }
)(FilterFormReduxForm);

export default FilterFormConnectReduxForm;

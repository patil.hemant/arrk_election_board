import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ReactPaginate from 'react-paginate';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';

const headCells = [
  { id: 'id', label: '#ID', isSortable: false },
  { id: 'name', label: 'Name', isSortable: false },
  { id: 'category', label: 'Category', isSortable: false },
  { id: 'no_of_votes', label: 'No Of Votes', isSortable: true },
  { id: 'no_of_eligible_users', label: 'No Of Eligible Users', isSortable: false },
  { id: 'voting_percentage', label: 'Voting Percentage', isSortable: false },
  { id: 'created_at', label: 'Created On', isSortable: true },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell, index) => (
          <TableCell
            key={`${headCell.id}-${index}`}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            {headCell.isSortable ? (
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            ) : (
                headCell.label
              )}

          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const SmartTable = props => {
  const { elections, electionMeta, handleSorting, handlePageNumberClick } = props;
  const classes = useStyles();
  const [order, setOrder] = useState('desc');
  const [orderBy, setOrderBy] = useState('created_at');

  const handleSortRequest = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
    handleSorting(property, isAsc ? 'desc' : 'asc');
  };

  const pageCount = Math.ceil(electionMeta.total_count / electionMeta.per);

  const handlePageClick = data => {
    let selected = data.selected;
    handlePageNumberClick(selected + 1);
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table>
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleSortRequest}
            />
            <TableBody>
              {elections.map(election => {
                return (
                  <TableRow
                    hover
                    key={election.id}
                  >
                    <TableCell>{election.id}</TableCell>
                    <TableCell>{election.name}</TableCell>
                    <TableCell>{election.category}</TableCell>
                    <TableCell>{election.no_of_votes}</TableCell>
                    <TableCell>{election.no_of_eligible_users}</TableCell>
                    <TableCell>{election.voting_percentage}</TableCell>
                    <TableCell>{election.created_at}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <ReactPaginate
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={'pagination page-numbering'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
        <span className='paginationTotalCount'>Total {electionMeta.total_count}</span>
      </Paper>
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  }
}));

export default SmartTable;

import React from 'react';

import ElectionsContainer from './ElectionsContainer';
import FilterForm from './FilterFormContainer';

const Elections = props => {
  return (
    <div>
      <h4>Listing Elections</h4>
      <FilterForm />
      <ElectionsContainer />
    </div>
  );
}
export default Elections;



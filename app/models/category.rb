class Category < ApplicationRecord
	has_many :elections

	validates :name, presence: true, length: { in: 2..25 }

end

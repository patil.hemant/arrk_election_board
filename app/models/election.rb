class Election < ApplicationRecord
	belongs_to :group
	belongs_to :category
	has_many :votes


	validates :name, presence: true


  def self.search(search_criteria, search_by)
    if search_by == "by_name"
      q = "%#{search_criteria}%"
      where('lower(name) LIKE ?',q.downcase)
    elsif search_by == "by_cat"
      id = Category.where(name: search_criteria).last.id
      where(category_id: id)
    else
      Election.all
    end
  end


  def no_of_candidates
    # Election.where("category_id IN (?)", self.category_id).all.map{|e|e.group.users.count}.flatten.uniq.count rescue 0
    self.group.users.count rescue 0
  end

  def voting_percentage
    total_no_of_votes = self.votes.count
    total_no_of_users = User.count
    # "#{(total_no_of_votes * 100.0 / (total_no_of_users))}%" rescue '0.0%'
    "#{total_no_of_votes.to_f/total_no_of_users.to_f * 100}%" rescue '0.0%' 
  end


end

class Vote < ApplicationRecord

	belongs_to :user
	belongs_to :election

	after_create :increment_election_no_of_votes
	before_destroy :decrement_election_no_of_votes

	def increment_election_no_of_votes
		election = self.election
		election.update_column('no_of_votes', election.no_of_votes.to_i + 1) if election.present?
	end

	def decrement_election_no_of_votes
		election = self.election
		election.update_column('no_of_votes', election.no_of_votes.to_i - 1) if election.present?
	end

end

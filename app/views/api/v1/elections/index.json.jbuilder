json.meta do
  json.total_count @total_items
  json.page @page
  json.per @per
end
json.data @elections do |election|
	json.id election.id
	json.name election.name
	json.category election.category.name
	json.no_of_votes election.no_of_votes
	json.no_of_eligible_users election.no_of_candidates
	json.voting_percentage election.voting_percentage
	json.created_at election.created_at.strftime("%A, %d %b %Y %l:%M %p")
end
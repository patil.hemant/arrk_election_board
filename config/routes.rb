Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :elections, only: [:index, :show]
    end
  end
  resources :elections
  root 'elections#index'
end

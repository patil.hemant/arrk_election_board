class CreateElections < ActiveRecord::Migration[6.0]
  def change
    create_table :elections do |t|
      t.string :name
      t.integer :category_id
      t.integer :group_id

      t.timestamps
    end
    add_index :elections, :category_id
    add_index :elections, :group_id
  end
end

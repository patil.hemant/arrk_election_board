class CreateVotes < ActiveRecord::Migration[6.0]
  def change
    create_table :votes do |t|
      t.integer :election_id
      t.integer :user_id

      t.timestamps
    end
    add_index :votes, :election_id
    add_index :votes, :user_id
  end
end

class AddNoOfVotesToElection < ActiveRecord::Migration[6.0]
  def change
    add_column :elections, :no_of_votes, :integer
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
Category.destroy_all
Group.destroy_all
Election.destroy_all
UserGroup.destroy_all
Vote.destroy_all

categories = ['College Representative', 'President', 'Vice President']
categories.each do |category|
	Category.create(name: category)
end

#Candidates
30.times do 
	User.create(
								name: Faker::Name.name,
								email: 	Faker::Internet.email
							)
end

groups = ['Group One', 'Group Two', 'Group Three', 'Group Four', 'Group Five', 'Group Six']
groups.each do |name|
	group = Group.create(
								name: name,
								description: Faker::Lorem
							)
end
	
User.all.each do | user |
	UserGroup.create(
										user_id: user.id,
										group_id: Group.pluck(:id).sample	
		)
end

#Voters
10.times do 
	User.create(
								name: Faker::Name.name,
								email: 	Faker::Internet.email
							)
end

Group.all.each do |group|
	2.times do	
		Election.create(
											name: Faker::Name.name, 
											group_id: group.id, 
											category_id: Category.pluck(:id).sample
										)
	end	
end

elections = Election.all
elections.all.each do |election|
	candidate_ids = election.group.users.pluck(:id)
	[4,8,9,12,15].sample.times do 
		Vote.create(
									election_id: election.id,
									user_id: candidate_ids.sample
								)
	end
end



# g1 = [1,2,3,4,5,6,7]  Candiates For President 
# g2 = [6,7,8,9,10,1,2]	Candiated For Vice President

# Election = President    g1 	
# 												g2 

# Election = Vice President
# 													g1
# 													g2		
# => 												g3
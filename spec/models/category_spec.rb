require 'rails_helper'

RSpec.describe Category, :type => :model do
  subject {
    described_class.new(name: "Ok",
                       )
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without a name" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "category name should not be too small" do
    subject.name = "a"
    expect(subject).to_not be_valid
  end

  it "category name should not be too long" do
    subject.name = "a" * 26
    expect(subject).to_not be_valid
  end

end
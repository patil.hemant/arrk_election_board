require 'rails_helper'

RSpec.describe Election, type: :model do
  describe 'associations' do
    it { should belong_to(:category).class_name('Category') }
    it { should belong_to(:group).class_name('Group') }
    it { should have_many(:votes) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end
end